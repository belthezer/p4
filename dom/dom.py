# -*- encoding: utf-8 -*-

import getpass
import re
import requests
import operator
import httplib2
from BeautifulSoup import BeautifulSoup

what = raw_input('Podaj czego szukasz: ')
where = raw_input('Podaj swoje miasto: ')
userAddress = raw_input('Podaj swój adres: ')
print "\n"
# userAddress = "Doroszewskiego 7, Warszawa"

def zamienZnaki(ciag):
    trans = dict((ord(a), b) for a, b in zip(u'ąćęłńóśżźĄĆĘŁŃÓŚŻŹ', u'acelnoszzACELNOSZZ'))
    return ciag.translate(trans)

def google(poczatek, koniec):
    nowyPoczatek = poczatek.replace(" ","+")
    nowyKoniec = koniec.replace(" ","+")

    sensor = 'false'
    language = 'pl'
    strona = 'http://maps.googleapis.com/maps/api/directions/json?origin='+nowyPoczatek+'&destination='+nowyKoniec+'&sensor='+sensor+'&language='+language

    strona = zamienZnaki(strona)
    session = requests.session()
    odp = session.get(strona)
    slownik = odp.json()
    try:
        dystans = slownik["routes"][0]["legs"][0]["distance"]["text"]
        odpowiedz = "\n\nOdleglosc wynosi: "+dystans+".\nZnaleziona trasa to: \n"
        steps = slownik["routes"][0]["legs"][0]["steps"]
        for cos in steps:
            przezIle = cos["distance"]["text"]
            komenda = cos["html_instructions"]
            komenda = re.sub('<[^<]+?>', '', komenda)
            odpowiedz += przezIle+" "+komenda+"\n"
        return odpowiedz
    except:
        print "Nie udało się wytyczyć trasy :("

# what = 'pub'
# where = 'warszawa'
strona = 'http://www.pkt.pl/pub/warszawa/3-1/?oWhat='+what+'&Where='+where
session = requests.session()
odp = session.get(strona)
pktHTML = odp.text
zupaPKT = BeautifulSoup(pktHTML)
slownikPKT = {"":""}
adresy = zupaPKT.findAll('h2')
if len(adresy) == 0:
    print "Podano bledne kryteria :("
else:
    for cos in adresy:
        nazwa = cos.findNext().text
        adres = cos.findNext().findNext().text
        if adres[0].isdigit():
            adres = adres[6:]
        slownikPKT.update({nazwa : adres})
        # print nazwa
        # print adres
    i = 0
    del slownikPKT[""]
    for nazwa in slownikPKT:
        print str(i)+": "+nazwa+", "+slownikPKT[nazwa]
        i+=1

    id = raw_input('\nWpisz numer interesującej cię pozycji: ')
    ctn = False
    while ctn == False:
        try:
            id = int(id)
            if id >= 0 and id <= i:
                ctn = True
            else:
                id = raw_input('Poadno zły zakres: ')
        except:
            id = raw_input('Nie poadno liczby. Wpisz ponownie: ')
    i = 0

    for cos in slownikPKT:
        if id == i:
            print google(userAddress, slownikPKT[cos])
        i+=1

