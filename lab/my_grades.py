# -*- encoding: utf-8 -*-

import getpass
import requests
import operator
import httplib2
from BeautifulSoup import BeautifulSoup

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'
}

username = raw_input('Użytkownik: ')
password = raw_input('Hasło: ')

payload = {
    'username': username,
    'password': password
}

# Logowanie
session = requests.session()
session.post('https://e.sggw.waw.pl/login/index.php',data=payload, verify=False)
response = session.get('http://e.sggw.waw.pl/grade/report/user/index.php?id=649', verify=False)
html = response.text

parsed = BeautifulSoup(html)
grades = parsed.find('table','user-grade')
nazwa = grades.findAll('img', title='Kategoria')[0]
nazwaPrzedmiotu = nazwa.findParent('td').text
nazwa = grades.findAll('img', title='Kategoria')[1]
Laboratoria = nazwa.findParent('td').text
nazwa = grades.findAll('img', title='Kategoria')[2]
PraceDomowe = nazwa.findParent('td').text
nazwa = grades.findAll('img', title='Kategoria')[3]
Aktywnosc = nazwa.findParent('td').text
print nazwaPrzedmiotu+"\n"

zadaniaLista = {"":""}

zadania = grades.findAll('img', title='Zadanie')
for zadanie in zadania:
    nazwaZadania = zadanie.findParent('td').text
    link = zadanie.findParent('td').find("a")["href"]
    ocenaZadania = zadanie.findNext().text
    ocenaZadania = ocenaZadania.replace(',','.')
    if (ocenaZadania == '-'): ocenaZadania = 0
    zadaniaLista.update({link+": "+nazwaZadania:float(ocenaZadania)})

print Laboratoria
del zadaniaLista[""]
sorted_x = sorted(zadaniaLista, key=zadaniaLista.get, reverse=True)
for cos in sorted_x:
    print cos+" "+str(zadaniaLista[cos])

warsztatyLista = {"":""}
warsztaty = grades.findAll('img', title='Warsztaty')
for warsztat in warsztaty:
    nazwaWarsztat = warsztat.findParent('td').text
    link = warsztat.findParent('td').find("a")["href"]
    ocenaWarsztaty = warsztat.findNext().text
    ocenaWarsztaty = ocenaWarsztaty.replace(',','.')
    if (ocenaWarsztaty == '-'): ocenaWarsztaty = 0
    warsztatyLista.update({link+": "+nazwaWarsztat:float(ocenaWarsztaty)})

print "\n"+PraceDomowe
del warsztatyLista[""]
sorted_x = sorted(warsztatyLista, key=warsztatyLista.get, reverse=True)
for cos in sorted_x:
    print cos+" "+str(warsztatyLista[cos])

foraLista = {"":""}
fora = grades.findAll('img', title='Forum')
for forum in fora:
    nazwaForum = forum.findParent('td').text
    link = forum.findParent('td').find("a")["href"]
    ocenaForum = forum.findNext().text
    ocenaForum = ocenaForum.replace(',','.')
    if (ocenaForum == '-'): ocenaForum = 0
    foraLista.update({link+": "+nazwaForum:float(ocenaForum)})

print "\n"+Aktywnosc
del foraLista[""]
sorted_x = sorted(foraLista, key=foraLista.get, reverse=True)
for cos in sorted_x:
    print cos+" "+str(foraLista[cos])
# Pobranie strony z ocenami

# Odczyt strony

# Parsowanie strony

# Scraping

# Sortowanie

# Wyświetlenie posortowanych ocen w kategoriach